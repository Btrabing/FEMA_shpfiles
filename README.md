## Read/Plot FEMA Regions 

This repository contains code to read in FEMA shapefiles and make a plot of the FEMA regions


**Written by \
Ben Trabing \
UCAR/NHC \
Ben.Trabing@noaa.gov**

Last updated May 23, 2024

## Required

    import geopandas as gpd
    import cartopy
    import matplotlib.pyplot as plt


## Directories:

    parm (input shapefiles)

    ush (scripts)

    figs (figures


